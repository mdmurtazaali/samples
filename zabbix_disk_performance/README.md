1. Copy userparameter_diskstats.conf to /etc/zabbix/zabbix_agentd.d/
2. Copy lld-disks.py /usr/local/bin
3. Add template to Configuration > Templates > Import

--
For test
$ zabbix_get -s <ip> -k "custom.vfs.discovery_disks"
